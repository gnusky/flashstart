package flashstart.flashstart.MVapp;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import flashstart.flashstart.MVapp.Controllers.Controller;
import flashstart.flashstart.MVapp.Controllers.Dispatchers.CertificateDispatcher;
import flashstart.flashstart.R;


public class Status extends AppCompatActivity {

    private boolean isOVPNInstalled;
    private boolean isProfileFileDownloaded;
    private boolean isConnected;
    private TextView ovpnTextView;
    private TextView profileTextView;
    private TextView connectionStatusTextView;
    private ImageButton ovpnButton;
    private ImageButton profileButton;
    private Button connectButton;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Controller.getController().checkPermissions(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_status);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.ovpnTextView = (TextView) findViewById(R.id.ovpnTextView);
        this.profileTextView = (TextView) findViewById(R.id.profileTextView);
        this.connectionStatusTextView = (TextView) findViewById(R.id.statusConnection);
        this.ovpnButton = (ImageButton) findViewById(R.id.ovpnButton);
        this.profileButton = (ImageButton) findViewById(R.id.profileButton);
        this.connectButton = (Button) findViewById(R.id.connectButton);
        this.connectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    onConnectButtonClick();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        this.ovpnButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    onOvpnButtonClick();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        this.profileButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    onProfileButtonClick();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        this.isConnected = false;
        checkStatus();
    }

    public void onResume() {
        super.onResume();  // Always call the superclass method first
        checkStatus();
    }


    public void onOvpnButtonClick() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(CertificateDispatcher.PlayStorePath + CertificateDispatcher.OpenVPNConnectString + CertificateDispatcher.PlayStoreGETLangParam)));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(CertificateDispatcher.BrowserPlayStorePath + CertificateDispatcher.OpenVPNConnectString + CertificateDispatcher.PlayStoreGETLangParam + Locale.getDefault().getLanguage())));
        }
    }

    public void onProfileButtonClick() {
        CertificateDispatcher.getCertificateDispatcher().getMobileCert(this);
        Toast.makeText(this, R.string.Downloading, Toast.LENGTH_SHORT).show();
        this.onPause();
    }

    public void onConnectButtonClick() {
        if (this.isOVPNInstalled && this.isProfileFileDownloaded) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setComponent(new ComponentName(CertificateDispatcher.OpenVPNConnectString, CertificateDispatcher.OpenVPNConnectActivityString));
            intent.setDataAndType(Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + CertificateDispatcher.CertificateFileName), CertificateDispatcher.OpenVPNApplicationStuff);
            startActivity(intent);
        }
    }

    public void checkStatus() {
        this.isOVPNInstalled = Controller.getController().isOpenVPNAppInstalled(getApplicationContext());
        this.isProfileFileDownloaded = Controller.getController().checkCertificateFileExistance(this);
        updateTextViews();
    }


    private void updateTextViews() {
        if (this.isOVPNInstalled) {
            this.ovpnTextView.setText(R.string.ovpnStatusOk);
            this.ovpnButton.setEnabled(false);
        } else {
            this.ovpnTextView.setText(R.string.ovpnStatusNotOk);
            this.ovpnButton.setEnabled(true);
        }
        if (this.isProfileFileDownloaded) {
            this.profileTextView.setText(R.string.ovpnProfileStatusOk);
            this.profileButton.setEnabled(false);
        } else {
            this.profileTextView.setText(R.string.ovpnProfileStatusNotOk);
            this.profileButton.setEnabled(true);
        }
        if (this.isConnected) {
            this.connectButton.setBackgroundColor(0xFF00FF00);
            this.connectionStatusTextView.setText(R.string.connectionStatusOK);
        } else {
            this.connectButton.setBackgroundColor(0xFFFF0000);
            this.connectionStatusTextView.setText(R.string.connectionStatusNotOK);
        }
        if (this.isOVPNInstalled && this.isProfileFileDownloaded) {
            this.connectButton.setEnabled(true);
        }
    }
}
