package flashstart.flashstart.MVapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import flashstart.flashstart.MVapp.Controllers.Controller;
import flashstart.flashstart.MVapp.Controllers.Dispatchers.UserDispatcher;
import flashstart.flashstart.MVapp.Model.Session;
import flashstart.flashstart.R;

public class Main extends AppCompatActivity {
    public static final String mySharedPreferences="FlashstartLoginPreferences";
    public static final String user = "username";
    public static final String pwd = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DE-NULLIFY NEEDED SINGLETONS
        Controller.getController();
        Session.getSession();
        UserDispatcher.getUserDispatcher();
        Controller.getController();
        switchMethod();
    }


    private void switchMethod() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(mySharedPreferences, MODE_PRIVATE);
        String tmpUser = pref.getString(user, "");
        String tmpPwd = pref.getString(pwd, "");
        if(!tmpUser.equals("") && !tmpPwd.equals("")) {
            UserDispatcher.getUserDispatcher().login(this,tmpUser,tmpPwd);
        } else {
            startLoginFragment();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        finish();
    }



    public void startStatusFragment() {
        Intent intent = new Intent(this, Status.class);
        startActivity(intent);
    }

    public void startLoginFragment() {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }
}
