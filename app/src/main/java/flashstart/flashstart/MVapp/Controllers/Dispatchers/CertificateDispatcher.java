package flashstart.flashstart.MVapp.Controllers.Dispatchers;

import android.os.Environment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileOutputStream;

import flashstart.flashstart.MVapp.Status;

/**
 * Created by Vishnu on 30/03/2017.
 */

public class CertificateDispatcher {
    private static final String MOBILE_URL = "http://cloud.flashstart.it/mobile";
    public final static String OpenVPNConnectString = "net.openvpn.openvpn";
    public final static String OpenVPNConnectActivityString = "net.openvpn.openvpn.OpenVPNAttachmentReceiver";
    public final static String OpenVPNApplicationStuff = "application/x-openvpn-profile";
    public final static String CertificateFileName = "mobileCertificate.ovpn";
    public final static String BrowserPlayStorePath = "https://play.google.com/store/apps/details?id=";
    public final static String PlayStorePath = "market://details?id=";
    public final static String PlayStoreGETLangParam = "&hl=";

    private static CertificateDispatcher certificateDispatcher;


    private CertificateDispatcher() {
    }

    public void getMobileCert(final Status main) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MOBILE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String filename = CertificateDispatcher.CertificateFileName;
                        File file = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOWNLOADS), filename);



                        try {
                            FileOutputStream stream = new FileOutputStream(file);
                            stream.write(response.toString().getBytes());
                            stream.close();
                            main.checkStatus();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {


        };

        RequestQueue requestQueue = Volley.newRequestQueue(main);
        requestQueue.add(stringRequest);
    }


    public static CertificateDispatcher getCertificateDispatcher() {
        if (certificateDispatcher == null) {
            certificateDispatcher = new CertificateDispatcher();
        }
        return certificateDispatcher;
    }


}
