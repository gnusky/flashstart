package flashstart.flashstart.MVapp.Controllers.Dispatchers;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import flashstart.flashstart.MVapp.Controllers.Controller;
import flashstart.flashstart.MVapp.Login;
import flashstart.flashstart.MVapp.Main;
import flashstart.flashstart.MVapp.Model.Session;
import flashstart.flashstart.MVapp.Model.User;
import flashstart.flashstart.R;

/**
 * Created by Vishnu on 08/03/2017.
 */

public class UserDispatcher {
    private static final String LOGIN_URL = "http://188.94.192.215/nic/update?";
    private static final String FORM_USERNAME="username=";
    private static final String FORM_PASSWORD="&password=";
    private static UserDispatcher userDispatcher;
    private static final String MOBILE_URL="http://cloud.flashstart.it/mobile/";


    private UserDispatcher() {

    }

    public void login(final AppCompatActivity main, final String username, final String password) {
        if(Controller.getController().isFormWellCompiled(username,password)){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, concatenateUrl(username, password),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.contains("good")) { //GOOD RESPONSE
                                Session.getSession().setCurrentUser(User.getUser(username, password));
                                ///////////////////////////////////////////////////////
                                if(main instanceof Main) {
                                    ((Main) main).startStatusFragment();
                                } else {
                                    ((Login) main).saveInCache();
                                    ((Login) main).startStatusFragment();
                                }
                            } else {
                                if(main instanceof Main) {
                                    ((Main) main).startLoginFragment();
                                } else {
                                     main.recreate();

                                }
                                Toast.makeText(main, R.string.error, Toast.LENGTH_SHORT).show();
                            }
                            ///////////////////////////////////////////////////////////////
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(main, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(main);
            requestQueue.add(stringRequest);
        }
    }

    private String concatenateUrl(final String username, final String password) {
        return (LOGIN_URL+FORM_USERNAME+username+FORM_PASSWORD+password);
    }

    public static UserDispatcher getUserDispatcher() {
        if(userDispatcher==null) {
            userDispatcher = new UserDispatcher();
        }
        return userDispatcher;
    }






}
