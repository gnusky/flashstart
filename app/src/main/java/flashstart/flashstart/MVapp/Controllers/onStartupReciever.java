package flashstart.flashstart.MVapp.Controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import flashstart.flashstart.MVapp.Login;

/**
 * Created by Vishnu on 05/04/2017.
 */

public class onStartupReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, Login.class);
            serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startService(serviceIntent);
        }
    }
}
