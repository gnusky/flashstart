package flashstart.flashstart.MVapp.Controllers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

import flashstart.flashstart.MVapp.Controllers.Dispatchers.CertificateDispatcher;

/**
 * Created by Vishnu on 03/04/2017.
 */

public class Controller {
    private static Controller controller;
    private static final int MIN_USER_LEN=5;
    private static final int MIN_PSW_LEN=6;
    public static final String USER_NAME = "username"; // key for username
    public static final String PASSWORD = "passoword";//key for password




    private Controller() {
    }

    public static Controller getController() {
        if(controller==null) {
            controller = new Controller();
        }
        return controller;
    }


    public boolean checkCertificateFileExistance(AppCompatActivity main) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+CertificateDispatcher.CertificateFileName);
        if(file.exists()) {
            return true;
        }
        return false;
    }


    public boolean isOpenVPNAppInstalled(Context context) {
        try {
            context.getPackageManager().getApplicationInfo(CertificateDispatcher.OpenVPNConnectString, 0);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }


    public boolean isFormWellCompiled(final String username, final String password) {
        if(username.length()>=MIN_USER_LEN && password.length()>=MIN_PSW_LEN) {
            return true;
        }
        if(username.length()<MIN_USER_LEN) {
            return false;
        }
        if(password.length()<MIN_PSW_LEN) {
            return false;
        }
        return false;
    }





    public void checkPermissions(AppCompatActivity main) {
        if(ContextCompat.checkSelfPermission(main,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(main,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    2);
        }
        if(ContextCompat.checkSelfPermission(main,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(main,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }
    }
}
