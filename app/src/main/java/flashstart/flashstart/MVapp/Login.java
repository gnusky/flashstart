package flashstart.flashstart.MVapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import flashstart.flashstart.MVapp.Controllers.Controller;
import flashstart.flashstart.MVapp.Controllers.Dispatchers.UserDispatcher;
import flashstart.flashstart.MVapp.Model.Session;
import flashstart.flashstart.R;

import static flashstart.flashstart.MVapp.Main.mySharedPreferences;

/**
 * A login screen that offers login via email/password.
 */
public class Login extends AppCompatActivity {

    private Button button;
    private TextView pwdForgotten;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_flashstart_main);
        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        button = (Button) findViewById(R.id.button);
        pwdForgotten = (TextView) findViewById(R.id.pwdForgotten);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    LoginButtonPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        pwdForgotten.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {

                } catch (Exception e) {

                }
            }
        });
        UserDispatcher.getUserDispatcher();
        Controller.getController();
        Session.getSession();
    }


    private void LoginButtonPressed() throws Exception {
        EditText user = (EditText) findViewById(R.id.Username);
        EditText pass = (EditText) findViewById(R.id.Password);
        final String username = user.getText().toString().trim();
        final String password = pass.getText().toString().trim();
        UserDispatcher.getUserDispatcher().login(this, username, password);
    }

    public void startStatusFragment() {
        Intent intent = new Intent(this, Status.class);
        startActivity(intent);
        finish();
    }

    public void saveInCache() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(mySharedPreferences, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(Main.user, Session.getSession().getCurrentUser().getUsername());
        prefsEditor.putString(Main.pwd, Session.getSession().getCurrentUser().getPassword());
        prefsEditor.apply();
    }



}

