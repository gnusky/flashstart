package flashstart.flashstart.MVapp.Model;

import java.util.Locale;

/**
 * Created by Vishnu on 08/03/2017.
 */

public class Session {

    private static Session session;
    private boolean isAdmin;
    private User currentUser;
    private String currentLanguage;
    private String responseString;

    private Session() {
        setAppLanguage();

    }

    public static Session getSession() {
        if (session == null) {
            session = new Session();
        }
        return session;
    }

    public void setResponseString(String string) {
        this.responseString=string;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(final User currentUser) {
        session.currentUser = currentUser;
    }

    private void setAppLanguage() {
        String sysLang = Locale.getDefault().getDisplayLanguage();
        if(sysLang.equals(Locale.ITALIAN.getDisplayLanguage())) {
            this.currentLanguage = Locale.ITALIAN.getDisplayLanguage();
        } else {
            this.currentLanguage = Locale.ENGLISH.getDisplayLanguage();
        }
    }
}

