package flashstart.flashstart.MVapp.Model;

/**
 * Created by Vishnu on 08/03/2017.
 */

public class User {
    private static User user;
    private String username;
    private String password;

    private User(final String username, final String password) {
        this.password = password;
        this.username = username;
    }


    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public static User getUser(final String username, final String password) {
        if(user == null) {
            user = new User(username, password);
        }
        return user;
    }

}
